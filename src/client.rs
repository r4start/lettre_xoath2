use lettre::smtp::client::net::NetworkStream;
use lettre::smtp::client::Client as SmtpClient;
use lettre::smtp::commands::*;
use lettre::smtp::extension::ClientId as SmtpClientId;
use lettre::smtp::response::Response as SmtpResponse;
use lettre::{ClientTlsParameters, EmailAddress, SendableEmail};
use lettre_email::Email;
use native_tls::{Protocol, TlsConnector};

use super::{Error, Xoauth2};

pub struct Client {
    client: Option<SmtpClient<NetworkStream>>,
}

impl Client {
    pub fn new() -> Self {
        Client { client: None }
    }

    pub fn connect(&mut self, server_address: &str, port: u16) -> Result<SmtpResponse, Error> {
        let mut tls_builder = TlsConnector::builder()?;
        tls_builder.supported_protocols(&[Protocol::Tlsv12])?;

        let tls_parameters =
            ClientTlsParameters::new(server_address.to_owned(), tls_builder.build()?);

        let mut email_client: SmtpClient<NetworkStream> = SmtpClient::new();
        let resp = email_client.connect(&(server_address, port), Some(&tls_parameters))?;

        if let Some(ref mut old_client) = self.client {
            old_client.close();
        }

        self.client = Some(email_client);
        Ok(resp)
    }

    pub fn is_connected(&mut self) -> bool {
        if let Some(ref mut client) = self.client {
            return client.is_connected();
        }

        false
    }

    pub fn is_encrypted(&self) -> bool {
        if let Some(ref client) = self.client {
            return client.is_encrypted();
        }

        false
    }

    pub fn close(&mut self) -> Result<(), Error> {
        if let Some(ref mut client) = self.client {
            client.close();
        }

        self.client = None;
        Ok(())
    }

    pub fn send(&mut self, email: &Email, auth: &Xoauth2) -> Result<(), Error> {
        if !self.is_connected() {
            return Ok(());
        }

        if let Some(ref mut email_client) = self.client {
            email_client.command(EhloCommand::new(SmtpClientId::new(
                "email-sender".to_owned(),
            )))?;

            email_client.command(auth)?;

            let from = if let Some(ref from) = email.envelope().from() {
                Some((*from).clone())
            } else {
                None
            };

            for addr in email.envelope().to().iter() {
                Client::send_to(email_client, from.clone(), addr.clone(), email.message())?;
            }
        }

        Ok(())
    }

    fn send_to(
        email_client: &mut SmtpClient<NetworkStream>,
        from: Option<EmailAddress>,
        to: EmailAddress,
        message: Box<&[u8]>,
    ) -> Result<(), Error> {
        email_client.command(MailCommand::new(from, vec![]))?;
        email_client.command(RcptCommand::new(to, vec![]))?;
        email_client.command(DataCommand)?;
        email_client.message(message)?;
        Ok(())
    }
}
