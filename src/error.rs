use std::convert::From;

use lettre::smtp::error::Error as SmtpError;
use native_tls::Error as NativeTlsError;

#[derive(Debug)]
pub enum Error {
    TlsError(NativeTlsError),
    SmtpClientError(SmtpError),
}

impl From<NativeTlsError> for Error {
    fn from(e: NativeTlsError) -> Self {
        Error::TlsError(e)
    }
}

impl From<SmtpError> for Error {
    fn from(e: SmtpError) -> Self {
        Error::SmtpClientError(e)
    }
}
