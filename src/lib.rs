extern crate base64;
extern crate lettre;
extern crate lettre_email;
extern crate native_tls;

mod client;
mod error;
mod xoauth2;

pub use self::client::Client;
pub use self::error::Error;
pub use self::xoauth2::Xoauth2;
