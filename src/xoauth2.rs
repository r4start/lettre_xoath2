use base64;
use std::fmt::{Display, Formatter, Result};

pub struct Xoauth2 {
    auth_data: String,
}

impl Xoauth2 {
    pub fn new(username: &str, token: &str) -> Self {
        let auth_data = format!("user={}\x01auth=Bearer {}\x01\x01", username, token);

        Xoauth2 {
            auth_data: base64::encode(&auth_data),
        }
    }
}

impl Display for Xoauth2 {
    fn fmt(&self, f: &mut Formatter) -> Result {
        write!(f, "AUTH XOAUTH2 {}\r\n", self.auth_data)
    }
}

#[cfg(test)]
mod test {
    use super::*;

    #[test]
    fn test_encoding() {
        let username: &str = "test@yandex.ru";
        let token: &str = "ArdFfigAAKFwEUbpZq1FQxufwJlrq-pE2g";
        let auth = Xoauth2::new(username, token);
        assert_eq!(auth.auth_data,
                   "dXNlcj10ZXN0QHlhbmRleC5ydQFhdXRoPUJlYXJlciBBcmRGZmlnQUFLRndFVWJwWnExRlF4dWZ3SmxycS1wRTJnAQE=");
    }
}
